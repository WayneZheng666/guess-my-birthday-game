from random import randint

user_name = input('Hi! What is your name?: ')
for i in range(5):
    i+=1
    user_input = input(f'Guess {i}: {user_name} were you born in {randint(1,12)}/{randint(1924, 2004)}?: ')

    if user_input.lower() == "yes":
        print('I knew it!')
    elif i==5:
        print('I have other things to do. Good bye')
    else:
        print('Drat! Lemme try again!')
